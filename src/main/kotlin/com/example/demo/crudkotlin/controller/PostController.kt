package com.example.demo.crudkotlin.controller

import com.example.demo.crudkotlin.model.CreatePostRequest
import com.example.demo.crudkotlin.model.PostResponse
import com.example.demo.crudkotlin.model.WebResponse
import com.example.demo.crudkotlin.service.PostServices
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class PostController(val postServices: PostServices) {
    @PostMapping(
        value = ["/api/posts"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun createPost(@RequestBody body: CreatePostRequest) : WebResponse <PostResponse> {
        val postResponse = postServices.create(body);
        return WebResponse(
            code = 200,
            status = "OK",
            data = postResponse
        )
    }

    @GetMapping(
        value = ["/api/posts/{idPost}"],
        produces = ["application/json"]
    )
    fun getPost(@PathVariable("idPost") id : Int) : WebResponse<PostResponse>{
        val postResponse = postServices.get(id)
        return WebResponse(
            code = 200,
            status = "OK",
            data = postResponse
            )
    }
    @PutMapping(
        value = ["/api/posts/{idPost}/update"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun updatePost(@PathVariable("idPost") id : Int,@RequestBody body : CreatePostRequest) : WebResponse<PostResponse>{
        val postResponse = postServices.update(id,body)
        return WebResponse(
            code = 200,
            status = "OK",
            data = postResponse
        )
    }

    @DeleteMapping(
    value = ["/api/posts/{idPost}/delete"],
    produces = ["application/json"]
    )
    fun deleteProduct (@PathVariable("idPost") id : Int ) : WebResponse<String>{
        val postResponse = postServices.delete(id)
        return WebResponse(
            code = 200,
            status = "OK",
            data = id.toString() + " Deleted",
        )
    }
}