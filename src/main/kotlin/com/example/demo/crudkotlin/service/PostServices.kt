package com.example.demo.crudkotlin.service

import com.example.demo.crudkotlin.model.CreatePostRequest
import com.example.demo.crudkotlin.model.PostResponse

interface PostServices {
    fun create(createPostRequest : CreatePostRequest) : PostResponse
    fun get(id : Int) : PostResponse
    fun update(id : Int,createPostRequest: CreatePostRequest) : PostResponse

    fun delete(id : Int)
}