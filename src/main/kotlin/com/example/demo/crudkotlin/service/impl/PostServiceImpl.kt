package com.example.demo.crudkotlin.service.impl

import com.example.demo.crudkotlin.entity.Post
import com.example.demo.crudkotlin.error.NotFoundException
import com.example.demo.crudkotlin.model.CreatePostRequest
import com.example.demo.crudkotlin.model.PostResponse
import com.example.demo.crudkotlin.repository.PostRepository
import com.example.demo.crudkotlin.service.PostServices
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class PostServiceImpl(val postRepository: PostRepository) : PostServices {
    override fun create(createPostRequest: CreatePostRequest): PostResponse {
        val post = Post(

            title = createPostRequest.title,
            description = createPostRequest.description,
            createdAt = Date(),
            updatedAt = null,
        )
        postRepository.save(post);
        return convertPostToPostResponse(post)
    }

    override fun get(id: Int): PostResponse {

        val post = postRepository.findById(id);
        if (post == null){
            throw NotFoundException()
        }
        else{
            return convertPostToPostResponse(post)
        }
    }

    override fun update(id: Int, createPostRequest: CreatePostRequest): PostResponse {
        val post = postRepository.findById(id)
        if (post == null){
            throw NotFoundException()
        }
        else{
            post.apply {
                title = createPostRequest.title !!
                description = createPostRequest.description
            }
            postRepository.save(post)
            return convertPostToPostResponse(post)
        }
    }

    override fun delete(id: Int) {
        val post = postRepository.findById(id)
        if (post == null){
            throw NotFoundException()
        }
        else{
            postRepository.delete(post)
        }

    }

    private fun convertPostToPostResponse (post : Post): PostResponse{
        return PostResponse(
            id = post.id,
            title = post.title,
            description = post.description
        )
    }
}