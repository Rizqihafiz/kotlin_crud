package com.example.demo.crudkotlin.model

data class PostResponse (
    val id : Int,

    val title: String,

    val description : String,

)