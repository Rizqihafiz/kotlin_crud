package com.example.demo.crudkotlin.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.*

@Entity
@Table(name = "post")
data class Post (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0 ,

    @Column(name = "title")
    var title: String ,

    @Column(name = "description")
    var description : String,

    @Column(name = "created_at")
    val createdAt : Date,

    @Column(name = "updated_at")
    val updatedAt : Date?,


)