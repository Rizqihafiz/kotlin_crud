package com.example.demo.crudkotlin.repository

import com.example.demo.crudkotlin.entity.Post
import org.springframework.data.jpa.repository.JpaRepository

interface PostRepository : JpaRepository<Post,String> {
    fun findById(id : Int?) : Post?
}